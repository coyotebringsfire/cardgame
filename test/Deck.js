const should = require('should');
const Deck = require('lib/Deck');

describe("Deck class", function() {
  const DECK = new Deck(52, function(count) {
    var a = new Array(count);
    for(let i=0; i<a.length; i++) {
      a[i] = Math.floor(Math.random()*100);
    }
    return a;
  });
  it(`should throw an exception if not passed the number of cards in the deck as the first argument`, function() {
    (function() {
      var deck = new Deck(function() {});
    }).should.throw();
  });
  it(`should throw an exception if not passed function as the second argument`, function() {
    (function() {
      var deck = new Deck(52);
    }).should.throw();
  });
  it(`should have a shuffle function`, function() {
    DECK.should.be.instanceof(Deck);
    DECK.shuffle.should.be.type('function');
  });
  it(`should have a draw function`, function() {
    DECK.should.be.instanceof(Deck);
    DECK.draw.should.be.type('function');
  });
  it(`should have a peek function`, function() {
    DECK.should.be.instanceof(Deck);
    DECK.peek.should.be.type('function');
  });

  describe(`Deck#shuffle`, function() {
    it("should randomize the card order", function() {
      var oldTopCard = DECK.cards[0];
      DECK.shuffle();
      oldTopCard.should.not.equal(DECK.cards[0]);
    });
  });

  describe(`Deck#draw`, function() {
    it("should remove the top card from the deck and return it", function() {
      var cards = DECK.draw(1);
      DECK.cards.length.should.equal(51);
    });
  });

  describe(`Deck#peek`, function() {
    it("should return the top card from the deck", function () {
      DECK.peek().should.equal(DECK.cards[0]);
    });
  });
});
