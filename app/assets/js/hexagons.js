var hexHeight,
    hexRadius,
    hexRectangleHeight,
    hexRectangleWidth,
    hexagonAngle = 0.523598776, // 30 degrees in radians
    sideLength = 30,
    boardWidth = 30,
    boardHeight = Math.floor(boardWidth * 9 / 16),
    cells = new Array(boardWidth);

(function(){
    var canvas = document.getElementById('hexmap');

    var ctx;

    hexWidth = 2*Math.sin(hexagonAngle) * sideLength;
    hexRadius = sideLength;
    hexRectangleHeight = sideLength + 2 * hexRadius;
    hexRectangleWidth = hexWidth;

    window.addEventListener("resize",resizeCanvas);

    function resizeCanvas() {
        const WINDOWPADDING = 8;
        canvas.width = window.innerWidth - 2*WINDOWPADDING;
        canvas.height = window.innerHeight - 2*WINDOWPADDING;
        sideLength = (canvas.width / boardWidth ) / 1.8;
        hexHeight = Math.ceil(Math.sin(hexagonAngle) * sideLength);
        hexRadius = Math.ceil(Math.cos(hexagonAngle) * sideLength);
        hexRectangleHeight = sideLength + 2 * hexHeight;
        hexRectangleWidth = 2 * hexRadius;

        if (canvas.getContext){
            ctx = canvas.getContext('2d');

            ctx.fillStyle = "#000000";
            ctx.strokeStyle = "#CCCCCC";
            ctx.lineWidth = 1;

            drawBoard(ctx, boardWidth, boardHeight);

            canvas.addEventListener("mousedown", onMouseDown);
        }
    }
    resizeCanvas();

    function onCellMouseDown(ctx, hexX, hexY, screenX, screenY) {
        ctx.fillStyle = "#00FF00";
        cells[hexX][hexY].strokecolor = ctx.fillStyle;
        console.log(`you clicked ${hexX}:${hexY}`);

        showCellInfo(ctx, hexX, hexY);
        ctx.fillStyle = cells[hexX][hexY].strokecolor;
        drawHexagon(ctx, screenX, screenY);
    }

    function showCellInfo(ctx, hexX, hexY) {
        const contentsCoords = { x:9, y:cells[boardWidth-1][boardHeight-1].center.y+Math.ceil(hexRadius*2)+15 };
        // TODO display info icon
        let img = new Image();
        img.onload = function () {
            imageWidthHeight = Math.ceil(hexRadius*2);
            ctx.drawImage(img, contentsCoords.x, contentsCoords.y, imageWidthHeight, imageWidthHeight);
        };
        img.src = `assets/img/cellinfo.png`;
        // TODO display icons for what you can do on this hex
    }

    function onMouseDown(eventInfo) {
        var x,
            y,
            hexX,
            hexY,
            screenX,
            screenY;

        x = eventInfo.offsetX || eventInfo.layerX;
        y = eventInfo.offsetY || eventInfo.layerY;


        hexY = Math.floor(y / (hexHeight + sideLength));
        hexX = Math.floor((x - (hexY % 2) * hexRadius) / hexRectangleWidth);

        screenX = hexX * hexRectangleWidth + ((hexY % 2) * hexRadius);
        screenY = hexY * (hexHeight + sideLength);

        if( hexY < boardHeight ) {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            drawBoard(ctx, boardWidth, boardHeight);
            // Check if the mouse's coords are on the board
            if(hexX >= 0 && hexX < boardWidth) {
                if(hexY >= 0 && hexY < boardHeight) {
                    onCellMouseDown(ctx, hexX, hexY, screenX, screenY);
                }
            }
        } else {
            // see if the click corresponds to any buttons
        }
    }

    function drawBoard(canvasContext, width, height) {
        var row,
            col;

        for(col = 0; col < width; ++col) {
            if( cells[col] === undefined ) {
                cells[col] = new Array(height);
            }
            for(row = 0; row < height; ++row) {
                if( cells[col][row] === undefined ) {
                    cells[col][row] = {
                        bgcolor: "#000000",
                        strokecolor: "#000000",
                        contents: [],
                        images: [],
                        center: {
                            x: col * hexRectangleWidth + ((row % 2) * hexRadius),
                            y: row * (sideLength + hexHeight)
                        }
                    };
                }
                ctx.fillStyle = cells[col][row].strokecolor || cells[col][row].bgcolor;
                drawHexagon(
                    ctx,
                    cells[col][row].center.x, //i * hexRectangleWidth + ((j % 2) * hexRadius),
                    cells[col][row].center.y); //j * (sideLength + hexHeight));
            }
        }
        for(col = 0; col < width; ++col) {
            for(row = 0; row < height; ++row) {
                for( image of cells[col][row].images ) {
                    let img = new Image();
                    let r = row, c = col;
                    img.onload = function () {
                        imageWidthHeight = Math.ceil(hexRadius*2);
                        let center = calculateHexCenter(c, r);
                        //console.log(`drawing image ${center.y} ${center.x} ${img.src}`);
                        ctx.drawImage(img, center.x-imageWidthHeight/2, center.y-imageWidthHeight/2, imageWidthHeight, imageWidthHeight);
                    };
                    img.src = `assets/img/${image}`;
                }
            }
        }
        // draw some boxes to isolate text
        ctx.beginPath();
        ctx.moveTo(4, cells[width-1][height-1].center.y+Math.ceil(hexRadius*2)+10);
        ctx.lineTo( boardWidth * Math.ceil(hexRadius*2), cells[width-1][height-1].center.y+Math.ceil(hexRadius*2)+10 );
        console.log(`board height ${boardHeight}`);
        ctx.lineTo( boardWidth * Math.ceil(hexRadius*2), canvas.height-10  );
        ctx.lineTo( 4, canvas.height-10 );
        ctx.closePath();
        ctx.stroke();
    }

    function calculateHexCenter(col,row) {
        return { x:cells[col][row].center.x+hexRadius, y:cells[col][row].center.y+hexRadius};
    }

    function drawHexagon(canvasContext, x, y) {
        var fill = fill || false;

        canvasContext.beginPath();
        canvasContext.moveTo(x + hexRadius, y);
        canvasContext.lineTo(x + hexRectangleWidth, y + hexHeight);
        canvasContext.lineTo(x + hexRectangleWidth, y + hexHeight + sideLength);
        canvasContext.lineTo(x + hexRadius, y + hexRectangleHeight);
        canvasContext.lineTo(x, y + sideLength + hexHeight);
        canvasContext.lineTo(x, y + hexHeight);
        canvasContext.closePath();

        canvasContext.stroke();
    }

})();

/*
 * @param overlay Array - an array of objects to be loaded
 *  each object must have hexX and hexY parameters describing which hex the object belongs in
 */
function loadOverlay(overlay) {
    //console.dir(overlay);
    for( let obj of overlay ) {
        cells[obj.hexX][obj.hexY].images.push(obj.image);
    }
    window.dispatchEvent(new Event("resize"));
    //console.log(`cells ${cells}`);
}
