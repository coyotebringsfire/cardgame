const should = require('should');
const Card = require('lib/Card');

describe("Card class", function() {
  it(`should throw an exception if not passed a object with options to the constructor`, function() {
    (function() {
      var card = new Card();
    }).should.throw();
  });
  it(`should construct a new object with the specified text`, function() {
    const TEST_TEXT = "This is a test";
    var card = new Card({ text: TEST_TEXT });
    card.text.should.equal(TEST_TEXT);
  });
});
