const DEFAULT_TEXT = "TEXT IS UNSET";

module.exports = class Card {
  constructor(options) {
    if( options === undefined ) {
      throw new Exception("missing required options");
    }
    this.text = options.text || DEFAULT_TEXT;
  }
}
