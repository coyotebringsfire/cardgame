class Deck {
  /*
   *
   * @param numberOfCards Integer
   * @param cardFactory function
   *
   */

  constructor(numberOfCards, cardFactory) {
    this.cards = cardFactory(numberOfCards);
  }
  shuffle() {
    let shuffle = require('knuth-shuffle').knuthShuffle;
    shuffle(this.cards);
  }
  draw(count) {
    let topCards = this.cards.slice(0,count);
    this.cards = this.cards.slice(count);
    return topCards;
  }
  peek() {
    return this.cards[0];
  }
}

module.exports = Deck;
